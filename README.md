# rest-example

Api Rest in Java - Base example to use in new exercises

## Run with docker
docker-compose up --build

### Run in localhost

http://localhost:9090/greeting

http://localhost:9090/greeting?name=Diego


## Run locally with gradle

gradle build

java -jar build/libs/rest-service-0.0.1-SNAPSHOT.jar

### Run in localhost

http://localhost:8080/greeting

http://localhost:8080/greeting?name=Diego



